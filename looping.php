<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping PHP</h1>
    <?php
    echo "<h3>LOOPING PERTAMA</h3>";
    for($i=1; $i<=19; $i+=2){
        echo $i . " - I LOVE PHP <br>";
    }

    echo "<h3>LOOPING KEDUA</h3>";
    for($a=19; $a>=1; $a-=2){
        echo $a . "- I LOVE PHP <br>";
    }

    echo "<h3>LOOPING ARRAY MODULO</h3>";
    $angka = [18, 45, 29, 61, 47, 34];
    echo "Array angka : ";
    print_r($angka);
    echo "<br>";
    echo "Sisa hasil bagi dari array : ";
    foreach($angka as $value){
    $rest[] = $value%-5;
    }
    print_r($rest);
    echo "<br>";

    echo "<h3>LOOP ASSOCIATE</h3>";
    $barang = [ 
    ["001" , "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
    ["002" , "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
    ["003" , "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
    ["004"  , "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"],
    ];
    foreach($barang as $key => $value){
    $items = array(
        "Id" => $value[0],
        "Nama" => $value[1],
        "Harga" => $value[2],
        "Deskripsi" => $value[3],
        "Source" => $value[4],
    );
    print_r($items);
    echo"<br>";
    }

    echo "<h3>LOOP ASTERIX</h3>";
    for ($j=1; $j<=5; $j++){ 
        for($b=1; $b<=$j; $b++){
        echo "*";
        }
    echo "Asterix:";
    echo "<br>";
    };

    ?>
</body>
</html>